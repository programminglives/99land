<?php
namespace Modules\Property\Models;

use App\BaseModel;

class PropertyPlotArea extends BaseModel
{
    protected $table = 'plot_area';
    protected $fillable = [
        'name',
    ];

    public static function getModelName()
    {
        return __("Property Plot Area");
    }

    public function property() {
        return $this->hasMany('Modules\Property\Models\Property','plot_area');
    }
}