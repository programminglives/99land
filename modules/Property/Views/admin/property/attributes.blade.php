@foreach ($attributes as $attribute)
    <div class="form-group">
        @if($attribute->name === 'Amenities')
        <strong>{{__(':name',['name'=>$attribute->name])}}</strong>
            <div class="terms-scrollable">
                @foreach($attribute->terms as $term)
                    <label class="term-item">
                        <input @if(!empty($selected_terms) and $selected_terms->contains($term->id)) checked @endif type="checkbox" name="terms[]" value="{{$term->id}}">
                        <span class="term-name">{{$term->name}}</span>
                    </label>
                @endforeach
            </div>
        @endif
    </div>
@endforeach