@if(is_default_lang())
<div class="form-group">
    <label class="control-label">{{__("State")}}<span style="color: red">*</span></label>
    @if(!empty($is_smart_search))
    <div class="form-group-smart-search">
        <div class="form-content">
            <?php
            $location_name = "";
            $list_json_state = [];
            $traverse = function ($states, $prefix = '')  {
                foreach ($states as $state) {
                    $list_json_state[] = [
                        'id' => $state->id,
                        'title' => $prefix . ' ' . $state->name,
                    ];

                }
            };
            $traverse($property_states);
            ?>
            <div class="smart-search">
                <input type="text" class="smart-search-location parent_text form-control" placeholder="{{__("-- Please Select --")}}" value="{{ $location_name }}" data-onLoad="{{__("Loading...")}}" data-default="{{ json_encode($list_json_state) }}">
                <input type="hidden" class="child_id" name="location_id" value="{{$row->location_id ?? Request::query('location_id')}}">
            </div>
        </div>
    </div>
    @else
    <div class="">
        <select name="state_id" class="form-control" id="state_id">
            <option value="">{{__("-- Please Select --")}}</option>
            <?php
            $traverse = function ($states, $prefix = '') use (&$traverse, $row) {
                foreach ($states as $state) {
                    $selected = '';
                    if ($row->location) {
                        if ($row->location->state_id == $state->id)
                            $selected = 'selected';
                    }
                    printf("<option value='%s' %s>%s</option>", $state->id, $selected, $prefix . ' ' . $state->name);
                }
            };
            $traverse($property_states);
            ?>
        </select>
    </div>
    @endif
</div>
@endif
@if(is_default_lang())
<div class="form-group">
    <label class="control-label">{{__("City")}}<span style="color: red">*</span></label>
    @if(!empty($is_smart_search))
    <div class="form-group-smart-search">
        <div class="form-content">
            <?php
            $location_name = "";
            $list_json = [];
            $traverse = function ($locations, $prefix = '') use (&$traverse, &$list_json, &$location_name, $row) {
                foreach ($locations as $location) {
                    $translate = $location->translateOrOrigin(app()->getLocale());
                    if ($row->location_id == $location->id) {
                        $location_name = $translate->name;
                    }
                    $list_json[] = [
                        'id' => $location->id,
                        'title' => $prefix . ' ' . $translate->name,
                    ];
                    $traverse($location->children, $prefix . '-');
                }
            };
            $traverse($property_location);
            ?>
            <div class="smart-search">
                <input type="text" class="smart-search-location parent_text form-control" placeholder="{{__("-- Please Select --")}}" value="{{ $location_name }}" data-onLoad="{{__("Loading...")}}" data-default="{{ json_encode($list_json) }}">
                <input type="hidden" class="child_id" name="location_id" value="{{$row->location_id ?? Request::query('location_id')}}">
            </div>
        </div>
    </div>
    @else
    <div class="">
        <select name="location_id" class="form-control" id="location_id">
            <option value="">{{__("-- Please Select --")}}</option>
            <?php
            $traverse = function ($locations, $prefix = '') use (&$traverse, $row) {
                foreach ($locations as $location) {
                    $selected = '';
                    if ($row->location_id == $location->id)
                        $selected = 'selected';
                    printf("<option value='%s' %s>%s</option>", $location->id, $selected, $prefix . ' ' . $location->name);
                    $traverse($location->children, $prefix . '-');
                }
            };
            $traverse($property_location);
            ?>
        </select>
    </div>
    @endif
</div>
@endif
<div class="form-group">
    <label class="control-label">{{__("Locality")}}</label>
    <input type="text" name="locality" class="form-control" placeholder="{{__("Locality")}}" value="{{$translation->locality}}">
</div>

<script>
$(function(){
    $('#state_id').prop('required', true);
    $('#location_id').prop('required', true);
});
  $(document).off('change', '#state_id').on('change', '#state_id', function(){
    var state = $(this).val();
    $('#location_id').empty();
    $.ajax({
                'url': '/property/cities/'+state,
                'cache': false,
                'type': 'GET',
                success: function (data) {
                   var place_holder_location = '<option></option>';
                   $('#location_id').append(place_holder_location);
                   city_data = data.data;
                   $.each(city_data, function( index, value ) {
                    var city_id_options = '<option value ='+city_data[index].id+'>'+city_data[index].title+'</option>'
                    $('#location_id').append(city_id_options);
                   });
                }
            });
        return false;
    });
</script>
