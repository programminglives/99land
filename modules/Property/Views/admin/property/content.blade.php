<?php

use Illuminate\Support\Facades\URL;
?>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<div class="panel">
    <div class="panel-title"><strong>{{__("Tell us about your property")}}</strong></div>


    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label"> {{__("List property for")}}<span style="color: red">*</span></label>
                    <select name="property_type" class="custom-select" id="property_type" required>
                        <option value="">{{__('-- Please select --')}}</option>
                        <option value="1" @if(old('property_type',$row->property_type ?? 0) == 1) selected @endif>{{__("Sell")}}</option>
                        <option value="2" @if(old('property_type',$row->property_type ?? 0) == 2) selected @endif>{{__("Rent")}}</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">{{__("Property Type")}}<span style="color: red">*</span></label>
                    <div class="">
                        <select name="category_id" class="form-control" id="category_id">
                            <option value="">{{__("-- Please Select --")}}</option>
                            <option value="1" @if(old('category_id',$row->category_id ?? 0) == 1) selected @endif>{{__("Apartment")}}</option>
                            <option value="2" @if(old('category_id',$row->category_id ?? 0) == 2) selected @endif>{{__("Flat")}}</option>
                            <option value="3" @if(old('category_id',$row->category_id ?? 0) == 3) selected @endif>{{__("Land/Plot")}}</option>
                            <option value="4" @if(old('category_id',$row->category_id ?? 0) == 4) selected @endif>{{__("House/Villa")}}</option>
                        </select>

                    </div>
                </div>
            </div>

        </div>


    </div>
</div>

<div class="panel">
    <div class="panel-title">
        <strong>
            {{__("Property Details")}}
        </strong>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>{{__("Title")}}</label>
                    <input type="text" value="{{$translation->title}}" placeholder="{{__("Name of the property")}}" name="title" class="form-control">
                </div>
                @include('Property::admin.property.location')

            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>{{__("Unit Type")}}</label>
                                <select name="unit_type" class="custom-select">
                                    <option value="">{{__('-- Please select --')}}</option>
                                    <option value="1" @if(old('unit_type',$row->unit_type ?? 0) == 1) selected @endif>{{__("1 BHK")}}</option>
                                    <option value="2" @if(old('unit_type',$row->unit_type ?? 0) == 2) selected @endif>{{__("2 BHK")}}</option>
                                    <option value="3" @if(old('unit_type',$row->unit_type ?? 0) == 3) selected @endif>{{__("3 BHK")}}</option>
                                    <option value="4" @if(old('unit_type',$row->unit_type ?? 0) == 4) selected @endif>{{__("4+ BHK")}}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>{{__("Built up")}}</label>
                                <input type="number" value="{{$row->build_up}}" placeholder="{{__("Example: 100")}}" name="build_up" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>{{__("Plot Area")}}</label>
                                <select name="plot_area" class="custom-select">
                                    <option value="">{{__('-- Please select --')}}</option>
                                    <option value="1" @if(old('plot_area',$row->plot_area ?? 0) == 1) selected @endif>{{__("Sq-ft")}}</option>
                                    <option value="2" @if(old('plot_area',$row->plot_area ?? 0) == 2) selected @endif>{{__("Sq-yrd")}}</option>
                                    <option value="3" @if(old('plot_area',$row->plot_area ?? 0) == 3) selected @endif>{{__("Sq-m")}}</option>
                                    <option value="4" @if(old('plot_area',$row->plot_area ?? 0) == 4) selected @endif>{{__("Acre")}}</option>
                                    <option value="5" @if(old('plot_area',$row->plot_area ?? 0) == 5) selected @endif>{{__("Bigha")}}</option>
                                    <option value="6" @if(old('plot_area',$row->plot_area ?? 0) == 6) selected @endif>{{__("Hectare")}}</option>
                                    <option value="7" @if(old('plot_area',$row->plot_area ?? 0) == 7) selected @endif>{{__("Marla")}}</option>
                                    <option value="8" @if(old('plot_area',$row->plot_area ?? 0) == 8) selected @endif>{{__("Kanal")}}</option>
                                    <option value="9" @if(old('plot_area',$row->plot_area ?? 0) == 9) selected @endif>{{__("Biswa1")}}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>{{__("Carpet Area")}}</label>
                                <input type="number" value="{{$row->area}}" placeholder="{{__("Example: 100")}}" name="area" class="form-control">
                            </div>
                        </div>
                    </div>
                    @if(is_default_lang())
                    <div class=“col-md-12”>
                        <div class=“form-group”>
                            <label>{{__("Property Price")}}<span style="color: red">*</span></label>
                            <input type="number" step="any" min="0" name="price" class="form-control" value="{{$row->price}}" placeholder="{{__("Property Price")}}" required>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label>{{__("Add Photos of your property")}}</label>
                    {!! \Modules\Media\Helpers\FileHelper::fieldGalleryUpload('gallery',$row->gallery) !!}
                </div>
            </div>

        </div>



    </div>
    @if(is_default_lang())
    <div class="panel">
        <div class="panel-title"><strong>{{__("More Information of your property")}}</strong></div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>{{__("About Property")}}</label>
                        <textarea id="content" class="form-control" name="content" placeholder="{{__("About Property (Min 30 Characters)")}}" rows="4" cols="50" value="{{$translation->content}}">
                        {{$translation->content}}
                        </textarea>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{__("Property On Floor")}}</label>
                                <input type="number" value="{{$row->property_on_floor}}" placeholder="{{__("Example: 3")}}" name="property_on_floor" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{__("Total Floor")}}</label>
                                <input type="number" value="{{$row->total_floor}}" placeholder="{{__("Example: 3")}}" name="total_floor" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{__("Furnishing")}}</label>
                                <select name="furnishing_type" class="custom-select">
                                    <option value="">{{__('-- Please select --')}}</option>
                                    <option value="1" @if(old('furnishing_type',$row->furnishing_type ?? 0) == 1) selected @endif>{{__("Unfurnished")}}</option>
                                    <option value="2" @if(old('furnishing_type',$row->furnishing_type ?? 0) == 2) selected @endif>{{__("Semi Furnished")}}</option>
                                    <option value="3" @if(old('furnishing_type',$row->furnishing_type ?? 0) == 3) selected @endif>{{__("Fully Furnished")}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{__("No. Bathroom")}}</label>
                                <input type="number" value="{{$row->bathroom}}" placeholder="{{__("Example: 5")}}" name="bathroom" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
    @endif


    @if(is_default_lang())
    <div class="panel">
        <div class="panel-title"><strong>{{__("Additional details")}}</strong></div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>{{__("No of Balconies")}}</label>
                        <select name="no_of_balconies" class="custom-select">
                            <option value="">{{__('-- Please select --')}}</option>
                            <option value="1" @if(old('no_of_balconies',$row->no_of_balconies ?? 0) == 1) selected @endif>{{__("1")}}</option>
                            <option value="2" @if(old('no_of_balconies',$row->no_of_balconies ?? 0) == 2) selected @endif>{{__("2")}}</option>
                            <option value="3" @if(old('no_of_balconies',$row->no_of_balconies ?? 0) == 3) selected @endif>{{__("3")}}</option>
                            <option value="4" @if(old('no_of_balconies',$row->no_of_balconies ?? 0) == 4) selected @endif>{{__("4")}}</option>
                            <option value="5" @if(old('no_of_balconies',$row->no_of_balconies ?? 0) == 5) selected @endif>{{__("5+")}}</option>
                            <option value="6" @if(old('no_of_balconies',$row->no_of_balconies ?? 0) == 6) selected @endif>{{__("None")}}</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{__("Direction")}}</label>
                        <select name="direction" class="custom-select">
                            <option value="">{{__('-- Please select --')}}</option>
                            <option value="1" @if(old('direction',$row->direction ?? 0) == 1) selected @endif>{{__("North")}}</option>
                            <option value="2" @if(old('direction',$row->direction ?? 0) == 2) selected @endif>{{__("South")}}</option>
                            <option value="3" @if(old('direction',$row->direction ?? 0) == 3) selected @endif>{{__("East")}}</option>
                            <option value="4" @if(old('direction',$row->direction ?? 0) == 4) selected @endif>{{__("West")}}</option>
                            <option value="5" @if(old('direction',$row->direction ?? 0) == 5) selected @endif>{{__("North East")}}</option>
                            <option value="6" @if(old('direction',$row->direction ?? 0) == 6) selected @endif>{{__("North West")}}</option>
                            <option value="7" @if(old('direction',$row->direction ?? 0) == 7) selected @endif>{{__("South East")}}</option>
                            <option value="8" @if(old('direction',$row->direction ?? 0) == 8) selected @endif>{{__("South West")}}</option>
                            <option value="9" @if(old('direction',$row->direction ?? 0) == 9) selected @endif>{{__("None")}}</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{__("Parking")}}</label>
                        <select name="parking" class="custom-select">
                            <option value="">{{__('-- Please select --')}}</option>
                            <option value="1" @if(old('parking',$row->parking ?? 0) == 1) selected @endif>{{__("4 Wheeler")}}</option>
                            <option value="2" @if(old('parking',$row->parking ?? 0) == 2) selected @endif>{{__("2 Wheeler")}}</option>
                            <option value="3" @if(old('parking',$row->parking ?? 0) == 3) selected @endif>{{__("2/4 Wheeler")}}</option>
                            <option value="4" @if(old('parking',$row->parking ?? 0) == 4) selected @endif>{{__("None")}}</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        @include('Property::admin.property.attributes')
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="panel">
        <div class="panel-title">
            <div class="row">
                <div class="col-md-4">
                    <strong><label>{{__('Property Features')}}</label></strong>
                </div>
                <div class="col-md-4">
                </div>
                <div class="col-md-4">
                    <strong><label>{{__('Terms and Conditions')}} </label></strong>

                </div>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">

                        <div class="row">
                            <div class="col-md-4">
                                <label>
                                    <input type="checkbox" name="is_featured" @if($row->is_featured) checked @endif value="1"> {{__("Enable featured")}}
                                </label>
                            </div>
                            <!-- <div class="col-md-6">
                                <label>
                                    <input type="checkbox" name="is_urgent" @if($row->is_urgent) checked @endif value="1"> {{__("Enable urgent")}}
                                </label>
                            </div> -->
                            <div class="col-md-6">
                            <label>
                            <input type="checkbox" name="is_top_property" @if($row->is_top_property) checked @endif value="1"> {{__("Enable Top Property")}}
                            </label>
                            </div>
                            <div class="col-md-2">
                           
                            </div>
                        </div>
                    </div>
                </div>
               
                  <!--   <div class="form-group">
                        <label>
                            <input type="checkbox" name="is_top_property" @if($row->is_top_property) checked @endif value="1"> {{__("Enable Top Property")}}
                        </label>
                    </div> -->
             
                <div class="col-md-6">
                    <div class="form-group">
                        <label>
                            By clicking Sign Up, you agree to our <a href="#"> Terms</a> and <a href="<?= URL::to('/'); ?>/privacy-policy" target="_blank">privacy Policy</a>.
                        </label>
                    </div>
                </div>
            </div>

        </div>


    </div>
</div>

@endif
<script>
    $(function() {
        $('#category_id').prop('required', true);
    });
    $(document).off('change', '#property_type').on('change', '#property_type', function() {
        var property_type = $(this).val();
        if (property_type == 2 || property_type == 1) {
            var category_id_options = '<option value =""> -- Please select --</option></option><option value =1>Apartment</option> <option value =2>Flat</option> <option value =4>House/Villa</option>'
            $('#category_id').empty();
            $('#category_id').append(category_id_options);
        }
        if (property_type == 1) {
            $('#category_id').append('<option value =3>Land/Plot</option>');
        }
        debugger;
        return false;
    });
</script>
