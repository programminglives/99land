<div class="container container-feature-property">
        <div class="row">
            <?php $size_col = 0; ?>
            @foreach($rows as $key=>$row)
                <?php
                    if(($key % 2) == 0 && $size_col != 0) {
                        $size_col = 3 ? 3 : 3;
                    }else {
                        $size_col = $size_col == 3 ? 3 : 3;
                    }
                ?>
                <div class="col-lg-{{$size_col}} col-xl-{{$size_col}}">
                @include('Property::frontend.layouts.search.loop-gird')
                </div>
            @endforeach
        </div>
        <div class="mbp_pagination feature-pagination">
        {{$rows->appends(request()->query())->links()}}
        </div>
    </div>

    <script>

    </script>