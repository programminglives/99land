<div class="container ovh container-top-property">
        @if(!empty(count($top_properties) > 0))
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <div class="main-title text-center mb40">
                    <h2>Top Property</h2>
                    <p>Get listed your's here.</p>
                </div>
            </div>
            </div>
            <div class="container">
        <div class="row">
            <?php $size_col = 0; ?>
            @foreach($top_properties as $key=>$row)
                <?php
                    if(($key % 2) == 0 && $size_col != 0) {
                        $size_col = 3 ? 3 : 3;
                    }else {
                        $size_col = $size_col == 3 ? 3 : 3;
                    }
                ?>
                <div class="col-lg-{{$size_col}} col-xl-{{$size_col}}">
                @include('Property::frontend.layouts.search.loop-gird')
                </div>
            @endforeach
        </div>
        <div class="mbp_pagination top-pagination">
        {{$top_properties->appends(request()->query())->links()}}
        </div>
    </div>
        @endif
        </div>