@php
    $translation = $row->translateOrOrigin(app()->getLocale());
@endphp
<div class="item">
    <div class="feat_property">
        <div class="thumb">
            @if ($row->getGallery())
                <?php $listGallery = $row->getGallery(); ?>
                <a href="{{$row->getDetailUrl()}}">
                    <img class="img-whp" src="{{$listGallery[0]['large']}}" alt="">
                </a>
            @else
                <span class="avatar-text-large">{{$row->vendor->display_name[0]}}</span>
            @endif
            <div class="thmb_cntnt">
                <ul class="tag mb0">
                    <li class="list-inline-item"><a href="{{$row->getDetailUrl()}}">{{$row->property_type == 1 ? _('Sell') : _('Rent')}}</a></li>
                    @if($row->is_featured && !$row->is_top_property)
                        <li class="list-inline-item"><a href="{{$row->getDetailUrl()}}">{{__('Featured')}}</a></li>
                    @else
                        <li class="d-none"></li>
                @endif
                <!-- @if($row->is_urgent && $row->is_top_property)
                    <li class="list-inline-item"><a href="{{$row->getDetailUrl()}}">{{__('Urgent')}}</a></li>
                @endif -->
                </ul>
                <ul class="icon mb0">
                    <li class="list-inline-item"><a class="service-wishlist @if($row->hasWishList) active @endif" data-id="{{$row->id}}" data-type="property"><i class="fa fa-heart"></i></a></li>
                    @if(auth()->user())
                        @if($row->create_user != auth()->user()->id)
                            <li class="list-inline-item">
                                <a class="service-message" data-id="{{$row->id}}" data-type="property">
                                    <i class="fa fa-commenting-o"></i>
                                </a>
                            </li>
                        @endif
                    @endif
                </ul>
                <a class="fp_price" href="{{$row->getDetailUrl()}}">{{ $row->display_price }}</a>
            </div>
        </div>
        <div class="details">
            <div class="tc_content">
                <div class = "row">
                    <div class = "col-md-4 col-4">
                        @if($row->Category)
                            <p class="text-thm">{{$row->Category->name}}</p>
                        @endif
                    </div>
                    @if(!empty($row->location->name))
                        @php $location =  $row->location->translateOrOrigin(app()->getLocale()) @endphp
                    @endif
                    <div class = "col-md-8 col-8 text-right">
                        <p><span class="flaticon-placeholder"></span> {{$location->name ?? ''}}</p>
                    </div>
                </div>

                <a @if(!empty($blank)) target="_blank" @endif href="{{$row->getDetailUrl()}}">
                    <h4><div class="hide-text-home">{{$translation->title}}</div></h4>
                </a>
            </div>
        </div>
    </div>
</div>
