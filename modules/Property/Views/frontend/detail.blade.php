@extends('layouts.app')
@section('head')

@endsection
@section('content')
@include('Property::frontend.layouts.details.gallery_property')
<?php
$str_to_replace = 'XXXXX';
$user = Illuminate\Support\Facades\Auth::user();
if(empty($user) && !empty($row->user->phone)) {
    $phone = $str_to_replace . substr($row->user->phone, 5);
} else {
    $phone = $row->user->phone ? $row->user->phone : '';
}?>

<!-- Agent Single Grid View -->
<section class="our-agent-single property-view bgc-f7 pb30-991">
    <div class="container">
        @include('Agencies::frontend.detail.search_mobile')
        <div class="row">
            <div class="col-md-12 col-lg-8 mt-5">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="listing_single_description2">
                          <div class="row">
                            <div class="col-lg-8 col-6">
                                <div class="single_property_title">
                                    <h3>{{ $translation->title ?? '' }}</h3>
                                </div>
                            </div>
                            <div class="col-lg-4 col-6 text-right">
                                <div class="single_property_social_share style2">
                                    <div class="price">
                                        <h2>{{ $row['display_price'] ? $row['display_price'] : '' }}</h2>
                                    </div>
                                </div>
                            </div>
                          </div>

                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="listing_single_description style2">
                            <div class="lsd_list mb0">
                                <ul class="mb0">
                                    @if(!empty($row['Category'])) <li class="list-inline-item"><a href="#">{{ $row['Category']->name}}</a></li> @endif
                                    <li class="list-inline-item"><a href="#">{{ __("City") }}: {{ !empty($row['Location']->name) ? $row['Location']->name : '' }}</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="additional_details">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h4 class="mb15">{{ __("Property Details") }}</h4>
                                </div>
                                <div class="col-12 col-md-6">
                                    <ul class="list-inline-item fa-ul list-color">
                                        <li>
                                          <span class="fa-li"><i class="fa fa-id-card-o" aria-hidden="true"></i></span>
                                            <p>{{ __("Property ID") }} : <span>{{ $row->id ? $row->id : 0 }}</span></p>

                                        </li>
                                        <li>
                                          <span class="fa-li"><i class="fa fa-money" aria-hidden="true"></i></span>
                                            <p>{{ __("Price") }} : <span>{{ $row->display_price ? $row->display_price : 0 }}</span></p>

                                        </li>
                                        <li>
                                          <span class="fa-li"><i class="fa fa-building" aria-hidden="true"></i></span>
                                            <p>{{ __("Build Up") }} : <span>{{ !empty($row['build_up']) ? $row['build_up']." ".($row->plotarea ? $row->plotarea->name : "") : 0}}</span></p>

                                        </li>
                                        <li>
                                          <span class="fa-li"><i class="fa fa-cubes" aria-hidden="true"></i></span>
                                            <p>{{ __("Total Floor") }} : <span>{{ $row->total_floor ? $row->total_floor : __('None') }}</span></p>

                                        </li>
                                    </ul>

                                </div>
                                <div class="col-12 col-md-6">
                                    <ul class="list-inline-item fa-ul list-color">
                                        <li>
                                          <span class="fa-li"><i class="fa fa-cubes" aria-hidden="true"></i></span>
                                            <p>{{ __("Property On Floor") }} : <span>{{ $row->property_on_floor ? $row->property_on_floor : 0 }}</span></p>

                                        </li>
                                        <li>
                                          <span class="fa-li"><i class="fa fa-bath" aria-hidden="true"></i></span>
                                            <p>{{ __("Bathrooms") }} : <span>{{ $row->bathroom ? $row->bathroom : 0 }}</span></p>

                                        </li>
                                        <li>
                                          <span class="fa-li"><i class="fa fa-snowflake-o" aria-hidden="true"></i></span>
                                            <p>{{ __("Unit Type") }} : <span>{{ $row->getUnitType($row->unit_type) }}</span></p>

                                        </li>
                                    </ul>

                                </div>
                                <div class="col-12 col-md-6">
                                    <ul class="list-inline-item fa-ul list-color">
                                        <li>
                                          <span class="fa-li"><i class="fa fa-address-book" aria-hidden="true"></i></span>
                                            <p>{{__('Property Type')}} : <span>{{$row->category ? $row->category->name : ''}}</span></p>

                                        </li>
                                        <li>
                                          <span class="fa-li"><i class="fa fa fa-bar-chart" aria-hidden="true"></i></span>
                                            <p>{{__('Property Status')}} : <span>{{$row->property_type == 1 ? __('Sell') : __('Rent')}}</span></p>

                                        </li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="additional_details">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h4 class="mb15">{{__('Additional details')}}</h4>
                                </div>
                                <div class="col-12 col-md-6">
                                    <ul class="list-inline-item fa-ul list-color">
                                        <li>
                                          <span class="fa-li"><i class="fa fa-building-o" aria-hidden="true"></i></span>
                                            <p>{{ __('No of Balconies') }} : <span>{{ $row->getBalconies($row->no_of_balconies) }}</span></p>

                                        </li>
                                        <li>
                                          <span class="fa-li"><i class="fa fa-arrow-right" aria-hidden="true"></i></span>
                                            <p>{{ __('Direction') }} : <span>{{ $row->getDirection($row->direction) }}</span></p>

                                        </li>
                                    </ul>

                                </div>
                                <div class="col-12 col-md-6">
                                    <ul class="list-inline-item fa-ul list-color">
                                        <li>
                                          <span class="fa-li"><i class="fa fa-bed" aria-hidden="true"></i></span>
                                            <p>{{ __('Furnishing') }} : <span>{{ $row->getFurnishingType($row->furnishing_type) }}</span></p>

                                        </li>
                                        <li>
                                          <span class="fa-li"><i class="fa fa-car" aria-hidden="true"></i></span>
                                            <p>{{ __('Parking') }} : <span>{{ $row->getParking($row->parking) }}</span></p>

                                        </li>
                                    </ul>

                                </div>
                                <div class="col-lg-12">
                                    <h4 class="mb30">{{ __("Description") }}</h4>
                                    <div class="collapse" id="collapseExample">
                                        <div class="card card-body">
                                            <p class="mt10 mb10">{!! clean($translation->content) !!}</p>
                                        </div>
                                    </div>
                                    <p class="overlay_close">
                                        <a class="text-thm fz14" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                            {{__('Show More')}} <span class="flaticon-download-1 fz12"></span>
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    @include('Property::frontend.layouts.details.property_feature')
                    @include('Property::frontend.layouts.details.property_video')
                    <div class="col-lg-12">
                        @include('Agencies::frontend.detail.review', ['review_service_id' => $row['id'], 'review_service_type' => 'property'])
                    </div>
                    @include('Property::frontend.layouts.details.property-related')
                </div>
            </div>
            <div class="col-lg-4 col-xl-4 mt50">
                <div class="sidebar_listing_list">
                    <div class="sidebar_advanced_search_widget">
                        <div class="sl_creator">
                            <h4 class="mb15">{{ __("Listed By") }}</h4>
                            <div class="media">
                                <a href="{{route('agent.detail',['id'=>$row->user->id])}}" class="c-inherit">
                                    @if($avatar_url = $row->user->getAvatarUrl())
                                    <img class="mr-3" src="{{$avatar_url}}" alt="{{$row->user->getDisplayName()}}">
                                    @else
                                    <span class="mr-3">{{ucfirst($row->user->getDisplayName()[0])}}</span>
                                    @endif
                                </a>
                                <div class="media-body">
                                    <h5 class="mt-0 mb0"><a href="{{route('agent.detail',['id'=>$row->user->id])}}" class="c-inherit">{{ $row->user->getDisplayName() }}</a></h5>
                                    @if(!empty($user))
                                        <p class="mb0">{{ !empty($phone) ? $phone : '' }}</p>
                                    @else
                                        <p class="mb0" id="phone_number" data-toggle="tooltip" title="Please login to contact this seller.">{{ !empty($phone) ? $phone : '' }}</p>
                                    @endif
                                    <p class="mb0">{{ !empty($row->user->email) ? $row->user->email : '' }}</p>
                                    <a class="text-thm" href="{{route('agent.detail',['id'=>$row->user->id])}}">{{ __("View My Listing") }}</a>
                                </div>
                            </div>
                        </div>
                        <form action="{{ route('agent.contact') }}" method="POST" class="agent_contact_form">
                            @csrf
                            <ul class="sasw_list mb0">
                                <li class="search_area">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="{{ __('Your Name') }}" name="name">
                                    </div>
                                </li>
                                <li class="search_area">
                                    <div class="form-group">
                                        <input type="number" class="form-control" placeholder="{{ __('Phone') }}" name="phone">
                                    </div>
                                </li>
                                <li class="search_area">
                                    <div class="form-group">
                                        <input type="email" class="form-control" id="email" placeholder="{{ __('Email') }}" name="email">
                                    </div>
                                </li>
                                <li class="search_area">
                                    <div class="form-group">
                                        <textarea id="form_message" name="message" class="form-control required" rows="5" placeholder="{{ __('Your Message') }}"></textarea>
                                    </div>
                                </li>
                                <li>
                                    <input type="hidden" name="vendor_id" value="{{ $row->user->id }}">
                                    <input type="hidden" name="object_id" value="{{ $row->id }}">
                                    <input type="hidden" name="object_model" value="property">
                                </li>
                                <li>
                                    <div class="search_option_button">
                                        <button type="submit" class="btn btn-block btn-thm">{{ __('Contact') }}</button>
                                    </div>
                                </li>
                            </ul>
                            <div class="form-mess"></div>
                        </form>
                    </div>
                </div>
                @include('Property::frontend.layouts.search.form-search')
                @include('Property::frontend.sidebar.FeatureProperty')
                @include('Property::frontend.sidebar.categoryProperty')
                @include('Property::frontend.sidebar.recentViewdProperty')
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
<link href="{{ asset('libs/fotorama/fotorama.css') }}" rel="stylesheet">
<script src="{{ asset('libs/fotorama/fotorama.js') }}"></script>
{!! App\Helpers\MapEngine::scripts() !!}
@endsection
