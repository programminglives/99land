<?php
use Illuminate\Support\Facades\Route;

Route::get('/','StateController@index')->name('state.admin.index');

Route::match(['get'],'/create','StateController@create')->name('state.admin.create');
Route::match(['get'],'/edit/{id}','StateController@edit')->name('state.admin.edit');

Route::post('/store/{id}','StateController@store')->name('state.admin.store');

Route::get('/getForSelect2','StateController@getForSelect2')->name('state.admin.getForSelect2');
Route::post('/bulkEdit','StateController@bulkEdit')->name('state.admin.bulkEdit');
