<?php

namespace Modules\State;

use Illuminate\Support\ServiceProvider;
use Modules\ModuleServiceProvider;

class ModuleProvider extends ModuleServiceProvider
{

    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/Migrations');
    }
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouterServiceProvider::class);
    }


    public static function getAdminMenu()
    {
        return [
            'location' => [
                "position" => 100,
                'url'        => 'admin/module/state',
                'title'      => __("State"),
                'icon'       => 'icon ion-md-map',

            ]
        ];
    }
    public static function getTemplateBlocks()
    {
        return [
            'list_states' => "\\Modules\\State\\Blocks\\ListStates",
        ];
    }
}
