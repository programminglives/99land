<?php

    namespace Modules\State\Models;

    use App\BaseModel;
    use Kalnoy\Nestedset\NodeTrait;
    use Modules\Booking\Models\Bookable;
    use Modules\Media\Helpers\FileHelper;
    use Illuminate\Database\Eloquent\SoftDeletes;
    use Modules\Core\Models\SEO;

    class State extends Bookable
    {
        use SoftDeletes;
        use NodeTrait;
        protected $table         = 'bravo_states';
        protected $fillable      = [
            'name',
        ];
        protected $slugFromField = 'name';
        protected $seo_type      = 'state';

        public static function getModelName()
        {
            return __("State");
        }

        public static function searchForMenu($q = false)
        {
            $query = static::select('id', 'name');
            if (strlen($q)) {

                $query->where('name', 'like', "%" . $q . "%");
            }
            $a = $query->limit(10)->get();
            return $a;
        }
    }
