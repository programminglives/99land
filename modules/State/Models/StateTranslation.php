<?php
namespace Modules\State\Models;

use App\BaseModel;

class StateTranslation extends BaseModel
{
    protected $table = 'bravo_state_translations';
    protected $fillable = ['name'];
    protected $seo_type = 'state_translation';

}
