<?php
namespace Modules\Contact\Controllers;

use App\Http\Controllers\Controller;

class PrivacyController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
        $data = [
            'page_title' => __("Privacy Policy"),
            'breadcrumbs'       => [
                [
                    'name'  => __('Privacy Policy'),
                    'url'  => route('privacy.index'),
                    'class' => 'active'
                ],
            ],
        ];
        return view('Contact::frontend.privacy.index', $data);
    }
}
