{{-- <div class="modal fade login" id="login" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content relative">
            <div class="modal-header">
                <h4 class="modal-title">{{__('Log In')}}</h4>
                <span class="c-pointer" data-dismiss="modal" aria-label="Close">
                    <i class="input-icon field-icon fa">
                        <img src="{{url('images/ico_close.svg')}}" alt="close">
                    </i>
                </span>
            </div>
            <div class="modal-body relative">
                @include('Layout::auth/login-form')
            </div>
        </div>
    </div>
</div>
<div class="modal fade login" id="register" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content relative">
            <div class="modal-header">
                <h4 class="modal-title">{{__('Sign Up')}}</h4>
                <span class="c-pointer" data-dismiss="modal" aria-label="Close">
                    <i class="input-icon field-icon fa">
                        <img src="{{url('images/ico_close.svg')}}" alt="close">
                    </i>
                </span>
            </div>
            <div class="modal-body">
                @include('Layout::auth/register-form')
            </div>
        </div>
    </div>
</div> --}}
{{-- <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"> --}}
<div class="sign_up_modal modal fade bd-example-modal-lg" id="login-register-tab" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              </div>
              <div class="modal-body container pb20">
                <div class="tab-content container" id="myTabContent">
                      <div class="row mt25 tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                          {{-- <div class="col-lg-6 col-xl-6">
                              <div class="login_thumb">
                                  <img class="img-fluid w100" src="{{ asset('/images/resource/login.jpg') }}" alt="login.jpg">
                              </div>
                          </div>--}}
                          <div class="col-lg-12 col-xl-12">
                            <div class="w3-container">
                                <div class="w3-row">
                                    <a href="javascript:void(0)" onclick="openTab(event, 'login');">
                                        <div class="w3-third tablink w3-bottombar w3-hover-light-grey w3-padding" id="defaultOpen">Login</div>
                                    </a>
                                    <a href="javascript:void(0)" onclick="openTab(event, 'register');">
                                        <div class="w3-third tablink w3-bottombar w3-hover-light-grey w3-padding">Register</div>
                                    </a>
                                </div>
                                <div  id="login" class="w3-container login-register" style="display:none">
                                    <div class="login_form"><br>
                                        @include('Layout::auth/login-form')
                                    </div>
                                </div>
                                <div id="register" class="w3-container login-register" style="display:none">
                                    <div class="sign_up_form"><br>
                                        @include('Layout::auth/register-form')
                                    </div>
                                </div>
                            </div>
                          </div>
                      </div>
                </div>
              </div>
        </div>
    </div>
</div>
<div class="sign_up_modal modal fade bd-example-modal-lg" id="success-msg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              </div>
              <div class="modal-body container pb20">
                <div class="tab-content container" id="myTabContent">
                      <div class="row mt25 tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="col-lg-12 col-xl-12">
                            <div class="login_form">
                            <h1>Registeration successful</h1>
                            <h3>Please check your email for verification</h3>
                            </div>
                          </div>
                      </div>
                </div>
              </div>
        </div>
    </div>
</div> 
{{-- <div class="sign_up_modal modal fade bd-example-modal-lg" id="login" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              </div>
              <div class="modal-body container pb20">
                <div class="tab-content container" id="myTabContent">
                      <div class="row mt25 tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                          <div class="col-lg-6 col-xl-6">
                              <div class="login_thumb">
                                  <img class="img-fluid w100" src="{{ asset('/images/resource/login.jpg') }}" alt="login.jpg">
                              </div>
                          </div>
                          <div class="col-lg-6 col-xl-6">
                            <div class="login_form">
                                @include('Layout::auth/login-form')
                            </div>
                          </div>
                      </div>
                </div>
              </div>
        </div>
    </div>
</div> 

 <div class="sign_up_modal modal fade bd-example-modal-lg" id="register" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              </div>
              <div class="modal-body container pb20">
                <div class="tab-content container" id="myTabContent">
                      <div class="row mt25 tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                          <div class="col-lg-6 col-xl-6">
                              <div class="login_thumb">
                                  <img class="img-fluid w100" src="{{ asset('/images/resource/login.jpg') }}" alt="login.jpg">
                              </div>
                          </div>
                          <div class="col-lg-6 col-xl-6">
                            <div class="sign_up_form">
                                <div class="heading">
                                    <h4>Register</h4>
                                </div>
                                @include('Layout::auth/register-form')
                            </div>
                          </div>
                      </div>
                </div>
              </div>
        </div>
    </div>
</div> --}}
<script>
function openTab(event, name) {
  var i, x, tablinks;
  x = document.getElementsByClassName("login-register");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" w3-border-red", "");
  }
  document.getElementById(name).style.display = "block";
  event.currentTarget.firstElementChild.className += " w3-border-red";
}
document.getElementById("defaultOpen").click();
</script>



