import ChatBox from "./components/ChatBox";
import Users from "./components/Users";

export default [
    {
        name: "ThreadMessage",
        path:'/messenger/:id',
        component:ChatBox
    },

    // {
    //     name: "MainMessagePage",
    //     path: '/messenger',
    //     component: ChatBox
    // }
]
