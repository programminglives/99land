<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AddProjectMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Update header menu
        $menu_items = array(
            array(
                'name'       => 'Home',
                'url'        => '/',
                'item_model' => 'custom',
                'model_name' => 'Custom',
                'children'   => array(),
            ),
            array(
                'name'       => 'Property List',
                'url'        => '/property',
                'item_model' => 'custom',
                'model_name' => 'Custom',
                'children'   => array(),
            ),
            array(
                'name'       => 'Agencies',
                'url'        => '#',
                'item_model' => 'custom',
                'model_name' => 'Custom',
                'children'   => array(
                    [
                        'name'       => 'Agencies',
                        'url'        => '/agency',
                        'item_model' => 'custom',
                        'model_name' => 'Custom',
                    ],
                    [
                        'name'       => 'Agent List',
                        'url'        => '/agent',
                        'item_model' => 'custom',
                        'model_name' => 'Custom',
                    ],
                ),
            ),
            array(
                'name'       => 'Projects',
                'url'        => '#',
                'item_model' => 'custom',
                'model_name' => 'Custom',
                'children'   => array(
                    [

                        'name'       => 'Upcoming Projects',
                        'url'        => '#',
                        'item_model' => 'custom',
                        'model_name' => 'Custom',
                    ],
                    [
                        'name'       => 'Ongoing Projects',
                        'url'        => '#',
                        'item_model' => 'custom',
                        'model_name' => 'Custom',
                    ],
                    [
                        'name'       => 'Completed Projects',
                        'url'        => '#',
                        'item_model' => 'custom',
                        'model_name' => 'Custom',
                    ]
                ),
            ),
        );

        DB::table('core_menus')->where('name', 'Main Menu')->update(['items' => json_encode($menu_items)]);
    }
}
