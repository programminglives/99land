<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class ImagePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $agent = Role::findOrCreate('agent');

        $agent->givePermissionTo('media_upload');
        $agent->givePermissionTo('media_manage');

        $agent = Role::findOrCreate('customer');

        $agent->givePermissionTo('media_upload');
        $agent->givePermissionTo('media_manage');
    }
}
