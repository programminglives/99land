<?php

use Illuminate\Database\Seeder;
use Modules\Location\Models\Location;
use Modules\Property\Models\Property;
use Modules\State\Models\State;

class UpdateCitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Update Hyderabad city
        $apHyderabadCity = Location::where('state_id', 1)->where('name','LIKE','%Hyderabad%')->get();
        $tsHyderabadCity = Location::where('state_id', 33)->where('name','LIKE','Hyderabad')->first();
        foreach($apHyderabadCity as $city) {
            $propertys = Property::where('location_id', $city->id)->get();
            if(!empty($propertys)) {
                foreach($propertys as $property) {
                    $property->update(['location_id' => $tsHyderabadCity->id]);
                }
            }
            $city->forceDelete();
        }
        $tsHyderabadCity->update(['image_id' => 130]);

        //Update delhi city
        $delhi = State::where('name', 'Delhi (DL)')->first();
        $dlCity = Location::where('id', 142)->first();
        $delhiCities = Location::where('state_id', $delhi->id)->whereNotIn('id', [$dlCity->id])->get();
        foreach($delhiCities as $delhiCity) {
            $propertys = Property::where('location_id', $delhiCity->id)->get();
            if(!empty($propertys)) {
                foreach($propertys as $property) {
                    $property->update(['location_id' => $dlCity->id]);
                }
            }
            $delhiCity->forceDelete();
        }
        $dlCity->update(['name' => 'Delhi']);
    }
}
