<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\State\Models\State;

class UpdateState extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Update Email Teplate
        DB::table('core_settings')->where(['name' => 'user_content_email_registered'])->update(['val' => '<h1 style="text-align: center">Welcome!</h1>
        <h3>Hello [first_name] [last_name]</h3>
        <p>Thank you for signing up with 99Land! We hope you enjoy your time with us.</p>
        <p>Please click button to verify your email address!</p>
        <p>[button_verify]</p>
        <p>Regards,</p>
        <p>99Land</p>']);
        DB::table('core_settings')->where(['name' => 'admin_content_email_user_registered'])->update(['val' => '<h3>Hello Administrator</h3>
        <p>We have new registration</p>
        <p>Full name: [first_name] [last_name]</p>
        <p>Email: [email]</p>
        <p>Regards,</p>
        <p>99Land</p>']);
        DB::table('core_settings')->where(['name' => 'user_content_email_forget_password'])->update(['val' => '<h1>Hello!</h1>
        <p>You are receiving this email because we received a password reset request for your account.</p>
        <p style="text-align: center">[button_reset_password]</p>
        <p>This password reset link expire in 60 minutes.</p>
        <p>If you did not request a password reset, no further action is required.
        </p>
        <p>Regards,</p>
        <p>99Land</p>']);
        DB::table('core_settings')->where(['name' => 'booking_enquiry_mail_to_vendor_content'])->update(['val' => '<h3>Hello [vendor_name]</h3>
        <p>You get new inquiry request from [email]</p>
        <p>Name :[name]</p>
        <p>Emai:[email]</p>
        <p>Phone:[phone]</p>
        <p>Content:[note]</p>
        <p>Service:[service_link]</p>
        <p>Regards,</p>
        <p>99Land</p>
        </div>']);
        DB::table('core_settings')->where(['name' => 'booking_enquiry_mail_to_admin_content'])->update(['val' => '<h3>Hello Administrator</h3>
        <p>You get new inquiry request from [email]</p>
        <p>Name :[name]</p>
        <p>Emai:[email]</p>
        <p>Phone:[phone]</p>
        <p>Content:[note]</p>
        <p>Service:[service_link]</p>
        <p>Vendor:[vendor_link]</p>
        <p>Regards,</p>
        <p>99Land</p>']);
        DB::table('core_settings')->where(['name' => 'vendor_content_email_registered'])->update(['val' => '<h1 style="text-align: center;">Welcome!</h1>
        <h3>Hello [first_name] [last_name]</h3>
        <p>Thank you for signing up with 99Land! We hope you enjoy your time with us.</p>
        <p>Please click button to verify your email address!</p>
        <p>[button_verify]</p>
        <p>Regards,</p>
        <p>99Land</p>']);
        DB::table('core_settings')->where(['name' => 'admin_content_email_vendor_registered'])->update(['val' => '<h3>Hello Administrator</h3>
        <p>An user has been registered as Vendor. Please check the information bellow:</p>
        <p>Full name: [first_name] [last_name]</p>
        <p>Email: [email]</p>
        <p>Registration date: [created_at]</p>
        <p>You can approved the request here: [link_approved]</p>
        <p>Regards,</p>
        <p>99Land</p>']);
        DB::table('core_settings')->where(['name' => 'invoice_company_info'])->update(['val' => '<p><span style="font-size: 14pt;"><strong>99Land Company</strong></span></p>
        <p>Ha Noi, Viet Nam</p>
        <p>www.99land.com</p>']);

        //Update Terms
        DB::table('bravo_terms')->where(['name' => 'Add Pooja Room'])->update(['name' => 'Pooja Room']);
        DB::table('bravo_terms')->where(['name' => 'Pool'])->update(['name' => 'Swimming Pool']);

        //Insert Telangana state
        DB::table('bravo_states')->insert([
            [
                'name'  => "Telangana (TS)",
                'create_user'   => 1,
                'created_at' => (new \DateTime())->format('Y-m-d H:i:s')
            ]
        ]);

        //Insert Telangana City
        $state = State::where('name', "Telangana (TS)")->first();
        $stateId = $state->id;
        DB::table('bravo_locations')->insert([
            [
                'name'  => "Hyderabad",
                'slug'   => 'hyderabad-3',
                'state_id' => $stateId,
                'map_lat' => '37.431572',
                'map_lng' => '-78.656891',
                'map_zoom' => '12',
                'status' => 'publish',
                '_lft' => '1305',
                '_rgt' => '1306',
                'create_user' => '1',
                'created_at' =>  date("Y-m-d H:i:s"),
                'updated_at' =>  date("Y-m-d H:i:s")
            ],
            [
                'name'  => "Warangal",
                'slug'   => 'warangal-1',
                'state_id' => $stateId,
                'map_lat' => '37.431572',
                'map_lng' => '-78.656891',
                'map_zoom' => '12',
                'status' => 'publish',
                '_lft' => '1307',
                '_rgt' => '1308',
                'create_user' => '1',
                'created_at' =>  date("Y-m-d H:i:s"),
                'updated_at' =>  date("Y-m-d H:i:s")
            ],
            [
                'name'  => "Nizamabad",
                'slug'   => 'nizamabad-1',
                'state_id' => $stateId,
                'map_lat' => '37.431572',
                'map_lng' => '-78.656891',
                'map_zoom' => '12',
                'status' => 'publish',
                '_lft' => '1309',
                '_rgt' => '1310',
                'create_user' => '1',
                'created_at' =>  date("Y-m-d H:i:s"),
                'updated_at' =>  date("Y-m-d H:i:s")
            ],
            [
                'name'  => "Khammam",
                'slug'   => 'khammam-1',
                'state_id' => $stateId,
                'map_lat' => '37.431572',
                'map_lng' => '-78.656891',
                'map_zoom' => '12',
                'status' => 'publish',
                '_lft' => '1311',
                '_rgt' => '1312',
                'create_user' => '1',
                'created_at' =>  date("Y-m-d H:i:s"),
                'updated_at' =>  date("Y-m-d H:i:s")
            ],
            [
                'name'  => "Karimnagar",
                'slug'   => 'karimnagar-1',
                'state_id' => $stateId,
                'map_lat' => '37.431572',
                'map_lng' => '-78.656891',
                'map_zoom' => '12',
                'status' => 'publish',
                '_lft' => '1313',
                '_rgt' => '1314',
                'create_user' => '1',
                'created_at' =>  date("Y-m-d H:i:s"),
                'updated_at' =>  date("Y-m-d H:i:s")
            ],
            [
                'name'  => "Ramagundam",
                'slug'   => 'ramagundam',
                'state_id' => $stateId,
                'map_lat' => '37.431572',
                'map_lng' => '-78.656891',
                'map_zoom' => '12',
                'status' => 'publish',
                '_lft' => '1315',
                '_rgt' => '1316',
                'create_user' => '1',
                'created_at' =>  date("Y-m-d H:i:s"),
                'updated_at' =>  date("Y-m-d H:i:s")
            ],
            [
                'name'  => "Mahabubnagar",
                'slug'   => 'mahabubnagar',
                'state_id' => $stateId,
                'map_lat' => '37.431572',
                'map_lng' => '-78.656891',
                'map_zoom' => '12',
                'status' => 'publish',
                '_lft' => '1317',
                '_rgt' => '1318',
                'create_user' => '1',
                'created_at' =>  date("Y-m-d H:i:s"),
                'updated_at' =>  date("Y-m-d H:i:s")
            ],
            [
                'name'  => "Nalgonda",
                'slug'   => 'nalgonda-1',
                'state_id' => $stateId,
                'map_lat' => '37.431572',
                'map_lng' => '-78.656891',
                'map_zoom' => '12',
                'status' => 'publish',
                '_lft' => '1319',
                '_rgt' => '1320',
                'create_user' => '1',
                'created_at' =>  date("Y-m-d H:i:s"),
                'updated_at' =>  date("Y-m-d H:i:s")
            ],
            [
                'name'  => "Adilabad",
                'slug'   => 'adilabad-1',
                'state_id' => $stateId,
                'map_lat' => '37.431572',
                'map_lng' => '-78.656891',
                'map_zoom' => '12',
                'status' => 'publish',
                '_lft' => '1321',
                '_rgt' => '1322',
                'create_user' => '1',
                'created_at' =>  date("Y-m-d H:i:s"),
                'updated_at' =>  date("Y-m-d H:i:s")
            ],
            [
                'name'  => "Siddipet",
                'slug'   => 'siddipet',
                'state_id' => $stateId,
                'map_lat' => '37.431572',
                'map_lng' => '-78.656891',
                'map_zoom' => '12',
                'status' => 'publish',
                '_lft' => '1323',
                '_rgt' => '1324',
                'create_user' => '1',
                'created_at' =>  date("Y-m-d H:i:s"),
                'updated_at' =>  date("Y-m-d H:i:s")
            ],
            [
                'name'  => "Miryalaguda",
                'slug'   => 'miryalaguda',
                'state_id' => $stateId,
                'map_lat' => '37.431572',
                'map_lng' => '-78.656891',
                'map_zoom' => '12',
                'status' => 'publish',
                '_lft' => '1325',
                '_rgt' => '1326',
                'create_user' => '1',
                'created_at' =>  date("Y-m-d H:i:s"),
                'updated_at' =>  date("Y-m-d H:i:s")
            ],
            [
                'name'  => "Suryapet",
                'slug'   => 'suryapet',
                'state_id' => $stateId,
                'map_lat' => '37.431572',
                'map_lng' => '-78.656891',
                'map_zoom' => '12',
                'status' => 'publish',
                '_lft' => '1327',
                '_rgt' => '1328',
                'create_user' => '1',
                'created_at' =>  date("Y-m-d H:i:s"),
                'updated_at' =>  date("Y-m-d H:i:s")
            ],
            [
                'name'  => "Jagtial",
                'slug'   => 'jagtial',
                'state_id' => $stateId,
                'map_lat' => '37.431572',
                'map_lng' => '-78.656891',
                'map_zoom' => '12',
                'status' => 'publish',
                '_lft' => '1329',
                '_rgt' => '1330',
                'create_user' => '1',
                'created_at' =>  date("Y-m-d H:i:s"),
                'updated_at' =>  date("Y-m-d H:i:s")
            ]
        ]);
    }
}
