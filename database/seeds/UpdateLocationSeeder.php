<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Location\Models\Location;

class UpdateLocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Update Locations
        DB::table('bravo_locations')->where(['name' => 'Bangalore Urban'])->update(['name' => 'Bangalore']);
        $location = Location::where('name', 'Bangalore Rural');
        $location->forceDelete();
        $locwithoutState = Location::where('state_id', 0);
        $locwithoutState->forceDelete();
    }
}
