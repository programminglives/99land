<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\State\Models\State;

class UpdateUPLocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $state = State::where('name', "Uttar Pradesh (UP)")->first();
        $stateId = $state->id;
        DB::table('bravo_locations')->insert([
            [
                'name'  => "Noida",
                'slug'   => 'noida',
                'state_id' => $stateId,
                'map_lat' => '37.431572',
                'map_lng' => '-78.656891',
                'map_zoom' => '12',
                'status' => 'publish',
                '_lft' => '1305',
                '_rgt' => '1306',
                'create_user' => '1',
                'created_at' =>  date("Y-m-d H:i:s"),
                'updated_at' =>  date("Y-m-d H:i:s")
            ],
        ]);
    }
}
