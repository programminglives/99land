<?php

use Illuminate\Database\Seeder;
use Modules\Media\Models\MediaFile;

class AmenitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (['Play Area', 'Tennis CourtX','Bank/Atm', 'Cafeteria','Library','Health Facilities','Temple'] as $k => $term) {
            $t = new \Modules\Core\Models\Terms([
                'name' => $term,
                'attr_id' => 2
            ]);
            $t->save();
        }
    }
}
