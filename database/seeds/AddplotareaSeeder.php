<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AddplotareaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plot_area')->insert([
            [
                'name'  => 'Sq-ft',
            ],
            [
                'name'  => 'Sq-yrd',
            ],
            [
                'name'  => 'Sq-m',
            ],
            [
                'name'  => 'Acre',
            ],
            [
                'name'  => 'Bigha',
            ],
            [
                'name'  => 'Hectare',
            ],
            [
                'name'  => 'Marla',
            ],
            [
                'name'  => 'Kanal',
            ],
            [
                'name'  => 'Biswa1',
            ],
        ]);
    }
}