<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class MapCustomerToPropertyPermission extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        $customer = Role::findOrCreate('customer');
        $customer->givePermissionTo('property_view');
        $customer->givePermissionTo('property_create');
        $customer->givePermissionTo('property_update');
        $customer->givePermissionTo('property_delete');
    }
}
