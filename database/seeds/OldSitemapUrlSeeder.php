<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OldSitemapUrlSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = 'public/json/post-sitemap.json';
        $content = json_decode(file_get_contents($path), true);
        foreach($content['data'] as $data) {
            DB::table('old_sitemap_urls')->insert([
                'url' => $data['URL'],
            ]);
        }
        $path = 'public/json/page-sitemap.json';
        $content = json_decode(file_get_contents($path), true);
        foreach($content['data'] as $data) {
            DB::table('old_sitemap_urls')->insert([
                'url' => $data['URL'],
            ]);
        }
    }
}
