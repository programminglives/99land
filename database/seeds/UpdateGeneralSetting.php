<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Media\Models\MediaFile;

class UpdateGeneralSetting extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('core_settings')->whereIn('group', ['general'])->delete();
        DB::table('core_settings')->insert([
            [
                'name'  => 'home_page_id',
                'val'   => '1',
                'group' => "general",
            ],
            [
                'name'  => 'page_contact_title',
                'val'   => "We'd love to hear from you",
                'group' => "general",
            ],
            [
                'name'  => 'page_contact_title_ja',
                'val'   => "あなたからの御一報をお待ち",
                'group' => "general",
            ],
            [
                'name'  => 'page_contact_sub_title',
                'val'   => "Send us a message and we'll respond as soon as possible",
                'group' => "general",
            ],
            [
                'name'  => 'page_contact_sub_title_ja',
                'val'   => "私たちにメッセージを送ってください、私たちはできるだ",
                'group' => "general",
            ],
            [
                'name'  => 'page_contact_desc',
                'val'   => "<!DOCTYPE html><html><head></head><body><h3>99land</h3><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>Tell. + 00 222 444 33</p><p>Email. hello@yoursite.com</p><p>1355 Market St, Suite 900San, Francisco, CA 94103 United States</p></body></html>",
                'group' => "general",
            ],
            [
                'name'  => 'page_contact_image',
                'val'   => MediaFile::findMediaByName("bg_contact")->id,
                'group' => "general",
            ]
        ]);
    // Setting Currency
    DB::table('core_settings')->insert([
            [
                'name'  => "currency_main",
                'val'   => "usd",
                'group' => "payment",
            ],
            [
                'name'  => "currency_format",
                'val'   => "left",
                'group' => "payment",
            ],
            [
                'name'  => "currency_decimal",
                'val'   => ",",
                'group' => "payment",
            ],
            [
                'name'  => "currency_thousand",
                'val'   => ".",
                'group' => "payment",
            ],
            [
                'name'  => "currency_no_decimal",
                'val'   => "0",
                'group' => "payment",
            ],
            [
                'name'  => "extra_currency",
                'val'   => '[{"currency_main":"eur","currency_format":"left","currency_thousand":".","currency_decimal":",","currency_no_decimal":"2","rate":"0.902807"},{"currency_main":"jpy","currency_format":"right_space","currency_thousand":".","currency_decimal":",","currency_no_decimal":"0","rate":"0.00917113"}]',
                'group' => "payment",
            ]
        ]);
    //MAP
    DB::table('core_settings')->insert([
            [
                'name'  => 'map_provider',
                'val'   => 'gmap',
                'group' => "advance",
            ],
            [
                'name'  => 'size_unit',
                'val'   => 'ft',
                'group' => "advance",
            ],
            [
                'name'  => 'map_gmap_key',
                'val'   => '',
                'group' => "advance",
            ]
        ]);
    // Payment Gateways
    DB::table('core_settings')->insert([
            [
                'name'  => "g_offline_payment_enable",
                'val'   => "1",
                'group' => "payment",
            ],
            [
                'name'  => "g_offline_payment_name",
                'val'   => "Offline Payment",
                'group' => "payment",
            ]
        ]);

        
        DB::table('core_settings')->insert(
            [
                [
                    'name' => 'site_title',
                    'val' => '99LAND',
                    'group' => "general"
                ],
                [
                    'name' => 'date_format',
                    'val' => 'd/m/Y',
                    'group' => "general"
                ],
                [
                    'name' => 'site_timezone',
                    'val' => 'Asia/Kolkata',
                    'group' => "general"
                ],                
                [
                    'name'  => 'menu_locations',
                    'val'   => '{"primary":1,"footer":2}',
                    'group' => "general",
                ],
                [
                    'name'  => 'admin_email',
                    'val'   => 'admin@99land.com',
                    'group' => "general",
                ], 
                [
                    'name'  => 'email_from_name',
                    'val'   => '99LAND',
                    'group' => "general",
                ], 
                [
                    'name'  => 'email_from_address',
                    'val'   => 'info@99land.com',
                    'group' => "general",
                ],
                [
                    'name'  => 'logo_id',
                    'val'   => MediaFile::findMediaByName("logo")->id,
                    'group' => "general",
                ],
                [
                    'name'  => 'logo_id_mb',
                    'val'   => MediaFile::findMediaByName("logo")->id,
                    'group' => "general",
                ],
                [
                    'name'  => 'logo_id_tran',
                    'val'   => MediaFile::findMediaByName("logo_trans")->id,
                    'group' => "general",
                ],
                [
                    'name'  => 'site_favicon',
                    'val'   => MediaFile::findMediaByName("favicon")->id,
                    'group' => "general",
                ],
                [
                    'name'  => 'topbar_left_text',
                    'val'   => '<div class="socials">
<a href="#"><i class="fa fa-facebook"></i></a>
<a href="#"><i class="fa fa-linkedin"></i></a>
<a href="#"><i class="fa fa-google-plus"></i></a>
</div>
<span class="line"></span>
<a href="mailto:info@99land.com">info@99land.com</a>',
                    'group' => "general",
                ],
                [
                    'name'  => 'footer_text_left',
                    'val'   => '© 99LAND. Exclusive For Property',
                    'group' => "general",
                ],
                [
                    'name'  => 'footer_text_right',
                    'val'   => '',
                    'group' => "general",
                ],
                [
                    'name'  => 'list_widget_footer',
                    'val'   => '{"1":{"title" : "About Site", "size" : "3", "content" : "<p>\r\n We’re reimagining how you buy, sell and rent. It’s now easier to get into a place you love. So let’s do this, together.\r\n </p>"},"2":{"title":"COMPANY","size":"3","content":"<ul>\r\n    <li><a href=\"/page/about-us\">About Us<\/a><\/li>\r\n    <li><a href=\"#\">Community Blog<\/a><\/li>\r\n    <li><a href=\"#\">Rewards<\/a><\/li>\r\n    <li><a href=\"#\">Work with Us<\/a><\/li>\r\n    <li><a href=\"#\">Meet the Team<\/a><\/li>\r\n<\/ul>"},
                    "3":{"title":"SUPPORT","size":"3","content":"<ul>\r\n    <li><a href=\"#\">Account<\/a><\/li>\r\n    <li><a href=\"#\">Legal<\/a><\/li>\r\n    <li><a href=\"/contact\">Contact<\/a><\/li>\r\n    <li><a href=\"#\">Affiliate Program<\/a><\/li>\r\n    <li><a href=\"#\">Privacy Policy<\/a><\/li>\r\n<\/ul>"},
                    "4":{"title":"Follow us","size":"3","content":"<ul class=\"mb30\">\r\n\t\t\t\t\t\t\t<li class=\"list-inline-item footer-social-icon\"><a href=\"#\"><i class=\"fa fa-facebook\"><\/i><\/a><\/li>\r\n\t\t\t\t\t\t\t<li class=\"list-inline-item footer-social-icon\"><a href=\"#\"><i class=\"fa fa-twitter\"><\/i><\/a><\/li>\r\n\t\t\t\t\t\t\t<li class=\"list-inline-item footer-social-icon\"><a href=\"#\"><i class=\"fa fa-instagram\"><\/i><\/a><\/li>\r\n\t\t\t\t\t\t\t<li class=\"list-inline-item footer-social-icon\"><a href=\"#\"><i class=\"fa fa-pinterest\"><\/i><\/a><\/li>\r\n\t\t\t\t\t\t\t<li class=\"list-inline-item footer-social-icon\"><a href=\"#\"><i class=\"fa fa-dribbble\"><\/i><\/a><\/li>\r\n\t\t\t\t\t\t\t<li class=\"list-inline-item footer-social-icon\"><a href=\"#\"><i class=\"fa fa-google\"><\/i><\/a><\/li>\r\n\t\t\t\t\t\t<\/ul>\r\n<h4>Subscribe<\/h4>\r\n<form class=\"footer_mailchimp_form\">\r\n\t\t\t\t\t\t \t<div class=\"form-row align-items-center\">\r\n\t\t\t\t\t\t\t    <div class=\"col-auto\">\r\n\t\t\t\t\t\t\t    \t<input type=\"email\" name=\"email\" class=\"form-control mb-2\" id=\"inlineFormInput\" placeholder=\"Your email\">\r\n\t\t\t\t\t\t\t    <\/div>\r\n\t\t\t\t\t\t\t    <div class=\"col-auto\">\r\n\t\t\t\t\t\t\t    \t<button type=\"submit\" class=\"btn btn-primary mb-2\"><i class=\"fa fa-angle-right\"><\/i><\/button>\r\n\t\t\t\t\t\t\t    <\/div>\r\n\t\t\t\t\t\t  \t<\/div>\r\n\t\t\t\t\t\t <div class=\"form-mess\"><\/div> <\/form>"}}',
                    'group' => "general",
                ],
                [
                    'name'  => 'contact_banner',
                    'val'   => MediaFile::findMediaByName("contact_banner")->id,
                    'group' => "general",
                ],
                [
                    'name'  => 'contact_partners_title',
                    'val'   => 'Become a Real Estate Agent',
                    'group' => "general",
                ],
                [
                    'name'  => 'contact_partners_sub_title',
                    'val'   => 'We only work with the best companies all over the india',
                    'group' => "general",
                ],
                [
                    'name'  => 'contact_partners_button_text',
                    'val'   => 'Register',
                    'group' => "general",
                ],
                [
                    'name'  => 'contact_partners_button_link',
                    'val'   => '/register',
                    'group' => "general",
                ],
                [
                    'name'  => 'list_widget_footer_ja',
                    'val'   => '[{"title":"\u52a9\u3051\u304c\u5fc5\u8981\uff1f","size":"3","content":"<div class=\"contact\">\r\n        <div class=\"c-title\">\r\n            \u304a\u96fb\u8a71\u304f\u3060\u3055\u3044\r\n        <\/div>\r\n        <div class=\"sub\">\r\n            + 00 222 44 5678\r\n        <\/div>\r\n    <\/div>\r\n    <div class=\"contact\">\r\n        <div class=\"c-title\">\r\n            \u90f5\u4fbf\u7269\r\n        <\/div>\r\n        <div class=\"sub\">\r\n            hello@yoursite.com\r\n        <\/div>\r\n    <\/div>\r\n    <div class=\"contact\">\r\n        <div class=\"c-title\">\r\n            \u30d5\u30a9\u30ed\u30fc\u3059\u308b\r\n        <\/div>\r\n        <div class=\"sub\">\r\n            <a href=\"#\">\r\n                <i class=\"icofont-facebook\"><\/i>\r\n            <\/a>\r\n            <a href=\"#\">\r\n                <i class=\"icofont-twitter\"><\/i>\r\n            <\/a>\r\n            <a href=\"#\">\r\n                <i class=\"icofont-youtube-play\"><\/i>\r\n            <\/a>\r\n        <\/div>\r\n    <\/div>"},{"title":"\u4f1a\u793e","size":"3","content":"<ul>\r\n    <li><a href=\"#\">\u7d04, \u7565<\/a><\/li>\r\n    <li><a href=\"#\">\u30b3\u30df\u30e5\u30cb\u30c6\u30a3\u30d6\u30ed\u30b0<\/a><\/li>\r\n    <li><a href=\"#\">\u5831\u916c<\/a><\/li>\r\n    <li><a href=\"#\">\u3068\u9023\u643a<\/a><\/li>\r\n    <li><a href=\"#\">\u30c1\u30fc\u30e0\u306b\u4f1a\u3046<\/a><\/li>\r\n<\/ul>"},{"title":"\u30b5\u30dd\u30fc\u30c8","size":"3","content":"<ul>\r\n    <li><a href=\"#\">\u30a2\u30ab\u30a6\u30f3\u30c8<\/a><\/li>\r\n    <li><a href=\"#\">\u6cd5\u7684<\/a><\/li>\r\n    <li><a href=\"#\">\u63a5\u89e6<\/a><\/li>\r\n    <li><a href=\"#\">\u30a2\u30d5\u30a3\u30ea\u30a8\u30a4\u30c8\u30d7\u30ed\u30b0\u30e9\u30e0<\/a><\/li>\r\n    <li><a href=\"#\">\u500b\u4eba\u60c5\u5831\u4fdd\u8b77\u65b9\u91dd<\/a><\/li>\r\n<\/ul>"},{"title":"\u8a2d\u5b9a","size":"3","content":"<ul>\r\n<li><a href=\"#\">\u8a2d\u5b9a1<\/a><\/li>\r\n<li><a href=\"#\">\u8a2d\u5b9a2<\/a><\/li>\r\n<\/ul>"}]',
                    'group' => "general",
                ],
                [
                    'name' => 'page_contact_title',
                    'val' => "We'd love to hear from you",
                    'group' => "general",
                ],
                [
                    'name' => 'page_contact_sub_title',
                    'val' => "Send us a message and we'll respond as soon as possible",
                    'group' => "general",
                ],
                [
                    'name' => 'page_contact_desc',
                    'val' => "<!DOCTYPE html><html><head></head><body><h3>99land</h3><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>Tell. + 00 222 444 33</p><p>Email. hello@yoursite.com</p><p>1355 Market St, Suite 900San, Francisco, CA 94103 United States</p></body></html>",
                    'group' => "general",
                ],
                [
                    'name' => 'page_contact_image',
                    'val' => MediaFile::findMediaByName("bg_contact")->id,
                    'group' => "general",
                ]
            ]
        );
    }
}
