<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Property\Models\PropertyCategory;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bravo_property_category')->truncate();
        $categories =  [
            ['name' => 'Apartment', 'content' => '', 'status' => 'publish',],
            ['name' => 'Builder Floor', 'content' => '', 'status' => 'publish',],
            ['name' => 'Land/Plot', 'content' => '', 'status' => 'publish',],
            ['name' => 'House/Villa', 'content' => '', 'status' => 'publish',]
        ];

        foreach ($categories as $category){
            $row = new PropertyCategory( $category );
            $row->save();
        }

    }
}
