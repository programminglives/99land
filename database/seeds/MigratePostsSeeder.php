<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;



class MigratePostsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          $posts = DB::connection('mysql2')->select(("SELECT * from `wp_posts` LIMIT 5"));
          foreach ($posts as $post) {
              $value = json_decode(json_encode($post), true);
              DB::table('bravo_properties')->insert([
                  'title' => $value['post_title'],
                  'location_id' => 1,
                  'category_id' => 1,
                  'price' => 900,
                  'slug' => $value['post_title'] ? str_replace(' ', '-', strtolower($value['post_title'])):-1,
                  'map_zoom' => 8,
                  'allow_children' => 0,
                  'allow_infant' => 0,
                  'garages' => 0,
                  'area' => 0,
                  'property_type'=>1,
                  'status' =>'publish',
                  'create_user' => 1,
              ]);
          }
    }
}
