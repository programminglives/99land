<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UpdatePropertyCategory extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bravo_property_category')
            ->where('id', 2)
            ->update([
                "name" => "Flat",
                "slug" => "flat",
            ]);
    }
}
