<?php

use GuzzleHttp\Client;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GetFBPostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client = new Client();

        $r = $client->request('Get', 'https://graph.facebook.com/v10.0/3753788378031405/feed?fields=message,message_tags,link,id,shares,type,place,attachments{media,title,type,url}&access_token=EAAEOjvlaAAkBAE3FNYVrrUEl6guAz3BMcdwsoaNjEKAAt9eeZC2M9NmMWj6ZAszo1biEjGZAvG9Cxtfvx3ndM4xPIexKXFuwWegPNLPJeg6M1qwsIGEZAsWDtLiZCHRmIQ6ZCnepiGTskm9HSeaywuPcKgoJFweAOL7iuYVicy3z4k8oPBWcYknkHZAJh1ZBUjh6jHQQloewmaOWEDW8cBY00ZCgNe9JvCX03wldR6A85ZB1qlydZAuWw3m');
 
        $response = json_decode($r->getBody()->getContents());
        $data = $response->data[0]->attachments->data[0];
        $value = explode(' ', $data->title);
        DB::table('bravo_properties')->insert([
            'title' => $value[4],
            'location_id' => 1,
            'category_id' => 1,
            'price' => 900,
            'slug' => $value[4] ? str_replace(' ', '-', strtolower($value[4])):-1,
            'area' => 0,
            'unit_type' => $value[0],
            'bathroom' => $value[2],
            'property_type'=>1,
            'status' =>'publish',
            'create_user' => 1,
        ]);
    }
}
