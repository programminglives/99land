<?php

use Illuminate\Database\Seeder;

class AddAdditionalAmenitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (['Add Pooja Room', 'Club House', 'Security', 'Community Hall', 'Garden'] as $k => $term) {
            $t = new \Modules\Core\Models\Terms([
                'name' => $term,
                'attr_id' => 2
            ]);
            $t->save();
        }
    }
}
