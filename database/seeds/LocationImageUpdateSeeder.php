<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LocationImageUpdateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Update Image Location
        DB::table('bravo_locations')->where(['id' => 511])->update(['image_id' => 125]);
        DB::table('bravo_locations')->where(['id' => 258])->update(['image_id' => 128]);
        DB::table('bravo_locations')->where(['id' => 20])->update(['image_id' => 130]);
        DB::table('bravo_locations')->where(['id' => 642])->update(['image_id' => 129]);
        DB::table('bravo_locations')->where(['id' => 366])->update(['image_id' => 126]);
        DB::table('bravo_locations')->where(['id' => 142])->update(['image_id' => 127]);
        //Update city name
        DB::table('bravo_locations')->where(['name' => 'Mumbai City'])->update(['name' => 'Mumbai']);
    }
}
