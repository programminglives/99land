<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessageThreadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('message_threads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('from_id')->unsigned();
            $table->bigInteger('to_id')->unsigned();
            $table->bigInteger('bravo_property_id')->unsigned();
            $table->timestamps();

            $table->foreign('from_id')->references('id')->on('users')
                ->onUpdate('cascade');
            $table->foreign('to_id')->references('id')->on('users')
                ->onUpdate('cascade');
            $table->foreign('bravo_property_id')->references('id')->on('bravo_properties')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('message_threads',function(Blueprint $table){
            $table->dropForeign(['from_id']);
            $table->dropForeign(['to_id']);
            $table->dropForeign(['bravo_property_id']);
        });
        Schema::dropIfExists('message_threads');
    }
}
