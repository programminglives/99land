<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('message');
            $table->bigInteger('message_thread_id')->unsigned();
            $table->bigInteger('sender_id')->unsigned();
            $table->boolean('seen')->default(false);
            $table->timestamps();

            $table->foreign('message_thread_id')->references('id')->on('message_threads')
                ->onUpdate('cascade');
            $table->foreign('sender_id')->references('id')->on('users')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('messages',function(Blueprint $table){
            $table->dropForeign(['sender_id']);
            $table->dropForeign(['message_thread_id']);
        });
        Schema::dropIfExists('messages');
    }
}
