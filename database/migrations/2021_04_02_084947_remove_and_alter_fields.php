<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


class RemoveAndAlterFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('bravo_properties', function (Blueprint $table) {
            $table->renameColumn('address', 'locality');
            $table->renameColumn('garages', 'unit_type');
            $table->renameColumn('square', 'build_up');
            $table->renameColumn('bed', 'property_on_floor');
            $table->renameColumn('year_built', 'total_floor');
            $table->renameColumn('remodal_year', 'furnishing_type');
            $table->renameColumn('pool_size', 'no_of_balconies');
            $table->renameColumn('additional_zoom', 'direction');
            $table->renameColumn('equipment', 'parking');
        });

        Schema::table('bravo_bookings', function (Blueprint $table) {
            if (Schema::hasColumn('sale_price', 'bravo_properties')) {
                $table->dropColumn('sale_price');
            }
            if (Schema::hasColumn('is_instant', 'bravo_properties')) {
                $table->dropColumn('is_instant');
            }
            if (Schema::hasColumn('allow_children', 'bravo_properties')) {
                $table->dropColumn('allow_children');
            }
            if (Schema::hasColumn('allow_infant', 'bravo_properties')) {
                $table->dropColumn('allow_infant');
            }
            if (Schema::hasColumn('max_guests', 'bravo_properties')) {
                $table->dropColumn('max_guests');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
