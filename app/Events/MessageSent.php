<?php

namespace App\Events;

use App\Message;
use App\MessageThread;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class MessageSent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $message;
    public $thread;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Message $message, MessageThread $thread)
    {
        Log::info($message);
        Log::info($thread);
        $this->thread = $thread;
        $this->message = $message;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        if ($this->message->sender_id == $this->thread->from_id){
            return new PrivateChannel('new.message.'.$this->thread->to_id);
        }
        return new PrivateChannel('new.message.'.$this->thread->from_id);
    }
}
