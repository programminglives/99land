<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OldSitemapUrl extends Model
{
    public $table = 'old_sitemap_urls';

    protected $fillable = ['url'];
}
