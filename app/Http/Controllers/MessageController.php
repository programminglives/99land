<?php

namespace App\Http\Controllers;

use App\Events\MessageSent;
use App\Message;
use App\MessageThread;
use App\User;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function connectedUsers(Request $request)
    {
        $messageThread = MessageThread::with(['userFrom','userTo'])->where('from_id',$request->user()->id)
            ->orWhere('to_id',$request->user()->id)->get();
        return $messageThread;
    }
    public function send(Request $request)
    {
        $message = Message::create([
            'sender_id' => $request->user()->id,
            'message_thread_id' => $request->message_thread_id,
            'message' => $request->message,
        ]);
        event(new MessageSent($message, MessageThread::find($request->message_thread_id)));
        return response([
            'message' => $message
        ],200);
    }
}
