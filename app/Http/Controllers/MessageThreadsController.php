<?php

namespace App\Http\Controllers;

use App\Message;
use App\MessageThread;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Modules\Property\Models\Property;

class MessageThreadsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        return view('messenger');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $property = Property::find($request->object_id);
        $thread = MessageThread::where('from_id',$request->user()->id)
            ->where('to_id',$property->create_user)
            ->where('bravo_property_id',$property->id)->first();
        if($thread)
            return $thread;
        $thread = MessageThread::create([
            'from_id' => $request->user()->id,
            'to_id' => $property->create_user,
            'bravo_property_id' => $property->id
        ]);
        return response($thread,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MessageThread  $messageThreads
     * @return \Illuminate\Http\Response
     */
    public function show(MessageThread $messageThreads)
    {
        $messages = Message::where('message_thread_id',$messageThreads->id)->get();
        return response([
            'data' => $messages,
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MessageThread  $messageThreads
     * @return \Illuminate\Http\Response
     */
    public function destroy(MessageThread $messageThreads)
    {
        Message::where('message_thread_id',$messageThreads->id)->delete();
        $messageThreads->delete();
        return response('Successfully Deleted!!!',200);
    }

    public function getConnectedUser(MessageThread $messageThread)
    {
        if($messageThread->from_id == auth()->user()->id)
            return User::find($messageThread->to_id);
        return User::find($messageThread->from_id);
    }
}
