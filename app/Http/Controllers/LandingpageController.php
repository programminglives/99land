<?php
/**
 * Created by PhpStorm.
 * User: dunglinh
 * Date: 6/8/19
 * Time: 23:47
 */
namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Modules\Booking\Models\Booking;
use OldSitemapUrl;

class LandingpageController extends Controller{

    public function index(){
        return view('landing.index');
    }

    public function detail($slug){
        if($slug !== 'admin') {
            $filterData = DB::table('old_sitemap_urls')->where('url','LIKE','%'.$slug.'%')
            ->get();
            if(!empty($filterData)) {
                $data = [
                    'slug' => $slug
                ];
                return view('landing.detail')->with($data);
            }else {
                abort(404);
            }
        }elseif($slug === 'admin') {
            $f = strtotime('monday this week');
            $data = [
                'recent_bookings'    => Booking::getRecentBookings(),
                'top_cards'          => Booking::getTopCardsReport(),
                'earning_chart_data' => Booking::getDashboardChartData($f, time())
            ];
            return view('Dashboard::index', $data);
        }
    }
}
