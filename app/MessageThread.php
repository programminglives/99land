<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageThread extends Model
{
    protected $fillable = ['from_id', 'to_id', 'bravo_property_id'];

    public function userFrom()
    {
        return $this->belongsTo(User::class,'from_id');
    }
    public function userTo()
    {
        return $this->belongsTo(User::class,'to_id');
    }
}
