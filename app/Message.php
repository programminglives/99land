<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['message', 'message_thread_id', 'seen', 'sender_id'];

    public function messageThread()
    {
        return $this->belongsTo(MessageThread::class,'message_thread_id');
    }
}
