<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use \Illuminate\Support\Facades\Route;
use \Illuminate\Support\Facades\Artisan;
use \Illuminate\Support\Facades\Auth;

Route::get('/intro','LandingpageController@index');
Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/install/check-db', 'HomeController@checkConnectDatabase');

Route::get('/update', function (){
    return redirect('/');
});

//Cache
Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cleared!";
});

//Login
Auth::routes(['verify' => true]);
//Custom User Login and Register
Route::post('register','\Modules\User\Controllers\UserController@userRegister')->name('auth.register');
Route::post('login','\Modules\User\Controllers\UserController@userLogin')->name('auth.login');
Route::post('logout','\Modules\User\Controllers\UserController@logout')->name('auth.logout');
// Social Login
Route::get('social-login/{provider}', 'Auth\LoginController@socialLogin');
Route::get('user/success', '\Modules\User\Controllers\UserController@success');
Route::get('social-callback/{provider}', 'Auth\LoginController@socialCallBack');
// Logs
Route::get('admin/logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->middleware(['auth', 'dashboard','system_log_view']);


Route::get('/messenger','MessageThreadsController@index')->name('messenger');
Route::post('/messenger/initiate','MessageThreadsController@store')->name('initiate.thread');
Route::get('messenger/{any}', 'MessageThreadsController@index')
    ->where('any','.*');
Route::get('/test',function(){
    $data = session()->get('jwt');
    return json_encode($data);
});
Route::get('/{slug}','LandingpageController@detail')->name('detail');
