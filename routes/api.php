<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'messenger', 'middleware' => 'auth:api'],function(){
    Route::get('/','MessageController@connectedUsers')->name('sendMessage');
    Route::get('/send-message','MessageController@send')->name('sendMessage');
    Route::get('/get-message/{messageThreads}','MessageThreadsController@show')->name('getMessages');
    Route::get('/delete-thread/{messageThreads}','MessageThreadsController@destroy')->name('deleteThread');
    Route::get('/connected-user/{messageThread}','MessageThreadsController@getConnectedUser')->name('connectedUser');

});
